from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from .forms import Login_Form

# Create your views here.
response = {}

def index(request):
    response['form'] = Login_Form()
    return render(request, 'login.html', response)

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        print("Authenticated")    
    else:
        print("Not Authenticated")    
    
    return HttpResponseRedirect('/login')


