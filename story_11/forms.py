from django import forms

class Login_Form(forms.Form):
    form_attrs = {'class':'form-control'}
    username = forms.CharField(label='From', max_length=30, strip=True, widget=forms.TextInput(attrs=form_attrs))
    password = forms.CharField(label='Message', max_length=300, strip=True, widget=forms.PasswordInput(attrs=form_attrs))
    