from django import forms

class Subs_Form(forms.Form):
    form_attrs = {'class':'form-control'}
    
    name = forms.CharField(label='Nama', max_length=30, strip=True, required=True, 
    widget=forms.TextInput(attrs=form_attrs))

    email = forms.EmailField(label='Email', required=True, 
    widget=forms.EmailInput(attrs=form_attrs))
    
    password = forms.CharField(label='Password', min_length=8, required=True, 
    widget=forms.PasswordInput(attrs=form_attrs))