$(function(){
    $('#id_email').change(function() {
        console.log("masuk change")
        var email = $(this).val();
        var form = $(this).closest('form');
        $.ajax({
            url: 'validate_email/',
            data: {
                'email': email,
            },
            dataType: 'json',
            success: function (data) {
                console.log("masuk ajax")
                if (data.is_taken) {
                    console.log("email is taken")
                    $('#subs-btn').prop('disabled', true);
                    console.log(data.error_message);
                    alert(data.error_message);
                } else {
                    $('#subs-btn').prop('disabled', false);
                }
            }
        });
    });

    $('form').on('submit', function(event){
        event.preventDefault();
        console.log('Form submitted!');
        var formData = $(this).serializeArray();
        $.ajax({
            type: 'POST',
            url: 'subscribe/',
            data: formData,
            dataType: 'json',
        }).done(function(){
            console.log('Subscribed')
            // $("#end-msg").html("You have been subscribed")
            // $("#end-msg").animate({
            //     'color' : '#008000',
            //     "font-size" : "15pt"
            // })
        }).fail(function(){
            console.log("Something's wrong")
            // $("#end-msg").html("You might've missed something")
            // $("#end-msg").animate({
            //     "font-size" : "15pt"
            // })
        })
    });

    $('.unsub-btn').click(function(){
        console.log('unsubs ' + $(this).attr('acc-id'));
        var acc_id = $(this).attr('acc-id');
        $.ajax({
            url: 'unsubs/',
            data: {
                'acc_id':acc_id,
            },
            dataType: 'json',
        }).done(function(){
            console.log('Unsubs');
            $(this).closest('.acc-card').slideUp();
        }).fail(function(){
            console.log("Something's wrong");
        })
    })
})