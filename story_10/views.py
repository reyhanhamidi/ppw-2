from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Subs_Form
from .models import Account

# Create your views here.
response = {}

def index(request):
    response['form'] = Subs_Form()
    return render(request, 'subs.html', response)

def subs_list(request):
    response['account_dict'] = Account.objects.all()[::-1]
    return render(request, 'subs_list.html', response)

def unsubscribe(request):
    acc_id = request.GET.get('acc_id', None)
    Account.objects.filter(id=acc_id).delete()
    data = {
        'success':(not Account.objects.filter(id=acc_id).exists())
    }
    return JsonResponse(data)

def subscribe(request):
    form = Subs_Form(request.POST or None)
    data = {}
    if (request.method == 'POST' and form.is_valid()):
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        account = Account(name=name, email=email, password=password)
        account.save()
        data['status'] = 'OK'
    else: 
        data['status'] = 'Failed'
    return JsonResponse(data)

def validate_email(request):
    email = request.GET.get('email', None)
    data = {
        'is_taken':Account.objects.filter(email__iexact=email).exists()
    }
    if data['is_taken']:
        data['error_message'] = 'A subscriber with this email already exists'        
    return JsonResponse(data)