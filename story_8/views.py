from django.shortcuts import render

# Create your views here.
response = {}
activity = [
    {
        "img": "ui.png",
        "title": 'Students of Computer Science',
        "desc": 'University of Indonesia class of 2017',
    },
    {
        "img": "ui.png",
        "title": 'Teaching Assistant',
        "desc": 'Teaching assistant of Introduction to Digital System for regular class',
    },
    {
        "img": "ui.png",
        "title": '(Calon) Wakil PJ Mentor dan Peserta BETIS 2019',
        "desc": 'Bimbingan belajar gratis yang diselenggarakan oleh Fasilkom UI',
    },
]

experience = [
    {
        "img": "ui.png",
        "title": 'Staff of Startup Academy',
        "desc": 'At COMPFEST X 2018, An IT event held by Computer Science University of Indonesia',
    },
    {
        "img": "ui.png",
        "title": 'Teaching Assistant',
        "desc": 'Teaching assistant of Introduction to Digital System for regular class',
    },
    # {
    #     "img": "ui.png",
    #     "title": '(Calon) Wakil PJ Mentor dan Peserta BETIS 2019',
    #     "desc": 'Bimbingan belajar gratis yang diselenggarakan oleh Fasilkom UI',
    # },
]

achievement = [
    {
        "img": "ui.png",
        "title": 'Staff of Startup Academy',
        "desc": 'At COMPFEST X 2018, An IT event held by Computer Science University of Indonesia',
    },
    {
        "img": "ui.png",
        "title": 'Teaching Assistant',
        "desc": 'Teaching assistant of Introduction to Digital System for regular class',
    },
    # {
    #     "img": "ui.png",
    #     "title": '(Calon) Wakil PJ Mentor dan Peserta BETIS 2019',
    #     "desc": 'Bimbingan belajar gratis yang diselenggarakan oleh Fasilkom UI',
    # },
]

accordion = [
    (activity, 'Activity'), 
    (experience, 'Experience'),
    (achievement, 'Achievement'),
]
response['accordion'] = accordion

def profile(request):
    return render(request, 'profile.html', response)