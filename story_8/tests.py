from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import profile

# Create your tests here.
def test_story_8_profile_url_is_exist(self):
    response = Client().get('/profile/')
    self.assertEqual(response.status_code,200)

def test_story_8_profile_using_correct_func(self):
    found = resolve('/profile/')
    self.assertEqual(found.func, profile)

def test_story_8_profile_using_correct_template(self):
    response = Client().get('/profile/')
    self.assertTemplateUsed(response, 'profile.html')