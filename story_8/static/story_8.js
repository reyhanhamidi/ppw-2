$(function(){
    $(".acc-desc").hide();

    $(".acc-title").click(function(){
        $(".acc-desc").not($(this).next()).slideUp();
        $(this).next().slideToggle();
    });
    
    $(".theme-switch span").click(function () {
       $("#dynamic-css").attr("href", $(this).attr("rel"));
    });

    // Loader
    setTimeout(showPage, 1000);

    function showPage() {
        $("#loader").css("display", "none");
        $(".container").css("display", "block");
    }
});