"""praktikum_2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from story_6.views import home, twatter, add_post
from story_8.views import profile
from story_9.views import library, get_api
from story_10.views import *
import story_10.urls as story_10
import story_11.urls as story_11


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name='home'),
    path('twatter/', twatter, name='twatter'),
    path('twatter/add_post/', add_post, name='add_post'),
    path('profile/', profile, name='profile'),    
    path('library/', library, name='library'),
    path('library/api/<slug:title>/', get_api, name='get_api_default'),
    path('subscription/', include(story_10)),
    path('login/', include(story_11)),
]   