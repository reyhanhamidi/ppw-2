from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create     your views here.
response = {}

def library(request):
    # https://www.googleapis.com/books/v1/volumes?q=quilting
    return render(request, 'library.html', response)

def get_api(request, title):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + title
    json_page = requests.get(url).json()
    return JsonResponse(json_page)