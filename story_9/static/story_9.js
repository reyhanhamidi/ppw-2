$(function(){
	search('quilting');
	
	function search(value){
		$(".section > .col").html('');
		// value = value.split(' ').join('%20');
   	$.ajax({
		url: "/library/api/" + value,
		success: function(result){
			console.log(result);
			var max_items = Math.max(result.items.length, 10);
			for (var i = 0; i < max_items; i++) {
				try {
					var book = result.items[i];
					var volume = book.volumeInfo;
					var title = volume.title;
					var author = volume.authors[0];
					var year = volume.publishedDate.slice(0,4);
					var image = volume.imageLinks.thumbnail;
					var desc = book.searchInfo.textSnippet;
					
					$(".section > .col").append(
						'<div class="row py-2 justify-content-center">\
						<div class="col-12 col-md-3 col-lg-2 text-center">\
					<img class="img-fluid" src="' + image + '">\
					</div>\
					<div class="col-12 col-md-7">\
					<b>' + title + '</b> (' + year +')<br>\
					by: ' + author + '\
					<p>' + desc + '</p>\
					<div class="col-md-auto p-0 fav-col">\
					<button type="button" class="btn btn-star btn-primary">\
					<i class="material-icons">star_border</i>\
					</button>\
					</div>\
					<div>\
					</div>\
					<hr>\
					'
					)
				}
				catch(err) {
					// var desc = '--no description--';
				}				
			}
			var fav_count = 0;
			$('.btn-star').click(function(){
				$(this).children().html(
					$(this).children().html() == 'star_border' ? 'star' : 'star_border'
				);
				fav_count += $(this).children().html() == 'star' ? 1 : -1;
				$('#fav_count').html(fav_count);
				console.log(fav_count);
			});

			
		}
	});
}
			$('#search-btn').click(function() {
				var search_title = $('#search-bar').val();
				console.log(search_title);
				search(search_title);
			});
});

