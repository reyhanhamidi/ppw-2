from selenium import webdriver
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from selenium.webdriver.firefox.options import Options
import time
from .views import library

# Create your tests here.

class Story6UnitTest(TestCase):
    def test_story_9_url_is_exist(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code,200)

    def test_story_9_using_correct_func(self):
        found = resolve('/library/')
        self.assertEqual(found.func, library)

    def test_story_9_using_correct_template(self):
        response = Client().get('/library/')
        self.assertTemplateUsed(response, 'library.html')

class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):        
        firefox_options = Options()
        firefox_options.add_argument('--headless')
        self.browser = webdriver.Firefox(firefox_options=firefox_options)
        # self.browser.maximize_window() # For maximizing window
        self.browser.get(self.live_server_url + '/twatter/')
        # self.browser.maximize_window() # For maximizing window
        # self.browser.implicitly_wait(20) # gives an implicit wait for 20 seconds
        time.sleep(7)

    def tearDown(self):
        self.browser.quit()

    # def test_can_add_favorite(self):
        
    #     frame = self.browser.find_element_by_xpath('//frame[@name="main"]')
    #     self.browser.switch_to.frame(frame)
    #     # pass1 = browser.find_element_by_id("PASSFIELD1")

    #     fav_count = self.browser.find_element_by_id('fav_count')
    #     self.assertTrue(fav_count, 0)

    #     fav_btn = self.browser.find_element_by_class_name('btn-star')
    #     fav_btn.click()
    #     self.assertTrue(fav_count, 1)
    #     fav_btn.click()
    #     self.assertTrue(fav_count, 0)
