from selenium import webdriver
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from selenium.webdriver.firefox.options import Options
from .views import twatter, home
from .models import Post
from .forms import Post_Form
import time

# Create your tests here.
class Story6UnitTest(TestCase):

    def test_story_6_twatter_url_is_exist(self):
        response = Client().get('/twatter/')
        self.assertEqual(response.status_code,200)

    def test_story_6_twatter_using_twatter_func(self):
        found = resolve('/twatter/')
        self.assertEqual(found.func, twatter)
    
    def test_story_6_twatter_using_correct_template(self):
        response = Client().get('/twatter/')
        self.assertTemplateUsed(response, 'twatter.html')

    # Models
    def test_model_can_create_model_object(self):
        Post.objects.create(name='Pewe', message='PPW Bisa!')
        num_of_all_objects = Post.objects.all().count()
        self.assertEqual(num_of_all_objects, 1)
        
    # Forms
    def test_forms_valid_has_data(self):
        form = Post_Form(data={
            'name': 'Barry Allen',
            'message': "I'm the fastest man alive",
        })
        self.assertTrue(form.is_valid())
        self.assertEqual(form.data['name'], "Barry Allen")
        self.assertEqual(form.data['message'], "I'm the fastest man alive")

    # View
    def test_post_and_display_success(self):
        response_post = Client().post(reverse('add_post'), data={
            'name': 'Jon Kent',
            'message': "Up, up and away!",
        })
        self.assertEqual(response_post.status_code,302)

        response = Client().post('/twatter/')

        html_response = response.content.decode('utf8')
        # html_response = render_to_string('home.html', request=request)
        self.assertIn('Jon Kent', html_response)
        self.assertIn('Up, up and away!', html_response)

    ## HOME
    def test_story_6_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story_6_home_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_story_6_home_using_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):        
        firefox_options = Options()
        firefox_options.add_argument('--headless')
        self.browser = webdriver.Firefox(firefox_options=firefox_options)
        # self.browser.maximize_window() # For maximizing window
        self.browser.get(self.live_server_url + '/twatter/')
        # self.browser.maximize_window() # For maximizing window
        # self.browser.implicitly_wait(20) # gives an implicit wait for 20 seconds
        time.sleep(10)

    def tearDown(self):
        self.browser.quit()

    def test_can_post_a_tweet(self):
        # The test is not necessarily "coba-coba" right?
        sender = 'Penguji Fungsional'
        msg = 'Ini adalah tes fungsional. Menurut wikipedia, \
        functional testing is a quality assurance (QA) process and a \
        type of black-box testing that bases its test cases on the \
        specifications of the software component under test.'

        # Memastikan belum ada post saat web dibuka
        self.assertNotIn(sender, self.browser.page_source)
        self.assertNotIn(msg, self.browser.page_source)

        from_box = self.browser.find_element_by_name('name')
        from_box.send_keys(sender)
        msg_box = self.browser.find_element_by_name('message')
        msg_box.send_keys(msg)
        from_box.submit()

        time.sleep(5)
        # Memastikan post sudah ditampilkan website
        self.assertIn(sender, self.browser.page_source)
        self.assertIn(msg, self.browser.page_source)

    def test_page_title(self):
        title_element = self.browser.find_element_by_tag_name('title')
        title = title_element.get_attribute('innerHTML')
        self.assertEquals(title, 'Tw@ter')

    def test_button_label(self):
        button_element = self.browser.find_element_by_tag_name('button')
        button_label = button_element.get_attribute('innerHTML')
        self.assertEquals(button_label, 'Tw@t')

    def test_body_background_color(self):
        body_element = self.browser.find_element_by_tag_name('body')
        body_color = body_element.value_of_css_property('background-color')
        self.assertEquals(body_color, 'rgba(0, 153, 255, 0.2)')

    def test_shadow_style_on_section_class(self):
        section_element = self.browser.find_element_by_class_name('section')
        section_value = section_element.value_of_css_property('box-shadow')
        self.assertEquals(section_value, 'rgba(0, 0, 0, 0.2) 0px 2px 4px 0px')