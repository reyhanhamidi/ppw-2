from django.db import models

# Create your models here.
class Post(models.Model):
    name = models.CharField(max_length=30, default='Anonymous')
    created_at = models.DateTimeField(blank=True, null=True)
    message = models.CharField(max_length=300, blank=False)
