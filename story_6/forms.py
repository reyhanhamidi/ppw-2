from django import forms

class Post_Form(forms.Form):
    form_attrs = {'class':'form-control'}
    name = forms.CharField(label='From', max_length=30, strip=True, widget=forms.TextInput(attrs=form_attrs))
    message = forms.CharField(label='Message', max_length=300, strip=True, widget=forms.Textarea(attrs=form_attrs))
    