from django.shortcuts import render
from django.utils import timezone
from django.http import HttpResponseRedirect
from .forms import Post_Form
from .models import Post

# Create your views here.
response = {}

def home(request):
    return render(request, 'home.html', response)

def twatter(request):
    form = Post_Form()
    response['form'] = form

    post_dict = Post.objects.all()[::-1]
    response['post_dict'] = post_dict
    return render(request, 'twatter.html', response)

def add_post(request):
    form = Post_Form(request.POST or None)
    response['form'] = form
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['created_at'] = timezone.now()
        response['message'] = request.POST['message']
        post = Post(name=response['name'],
            created_at=response['created_at'],
            message=response['message']
        )
        post.save()
    
    return HttpResponseRedirect('/twatter')